const connect = require("../db/connect");

module.exports = class dbController {
  /*Consulta para obter a lista de tabelas (show tables) */
  static async getTables(req, res) {
    const queryShowTables = "show tables";
    connect.query(queryShowTables, async function (err, result, fields) {
      /* Extrai os nomes da tabela de forma organizada */
      const tableNames = result.map((row) => row[fields[0].name]);
      if (err) {
        console.log("Erro: " + err);
        return res
          .status(500)
          .json({ error: "Erro ao obter tabelas da database" });
      }
      res
        .status(200)
        .json({ message: "Estas são as tabelas da database: ", tableNames });
      console.log("Tabelas do banco de dados: ", tableNames);
    });
  } /* fim getTables */

  static async getTablesDescription(req, res) {
    const queryShowTablesDescription = "show tables";

    connect.query(
      queryShowTablesDescription,
      async function (err, result, fields) {
        if (err) {
          console.log("Erro: " + err);
          return res
            .status(500)
            .json({ error: "Erro ao obter tabelas da database" });
        }

        /* Extrai os nomes da tabela de forma organizada */
        const tableNames = result.map((row) => row[fields[0].name]);

        //res.status(200).json({message: "Tabelas do banco - forma bruta: ", result, tables: tableNames})
        console.log("Tabelas do banco de dados: ", tableNames);

        //Organização e descrição das tabelas do banco
        const tables = [];

        //Iterar sobre os resultados para obter a descrição dee cada tabela
        for (let i = 0; i < result.length; i++) {
          //Analisando o banco através de seus atributos
          const tableName =
            result[i][`Tables_in_${connect.config.connectionConfig.database}`];

          //Acionando o comando desc
          const queryDescTable = `describe ${tableName} `;

          try {
            const tableDescription = await new Promise((resolve, reject) => {
              connect.query(queryDescTable, function (err, result, fields) {
                if (err) {
                  reject(err);
                }
                resolve(result);
              });
            }); //Fim da const

            tables.push({
              name: tableName,
              description: tableDescription,
            });
          } catch (error) {
            console.log(error);
            return res
              .status(500)
              .json({ error: "Erro ao obter a descrição da tabela!" });
          }
        }

        res
          .status(200)
          .json({
            message: "Obtendo todas as tabelas de suas descrições",
            tables,
          });
      }
    );
  } /*fim getTablesDEscription */

  /*Consulta para obter a descrição da tabela usuário */
  static async descUsuario(req, res) {
    const queryDescUsuario = "desc usuario";

    connect.query(queryDescUsuario, async function (err, result, fields) {
      if (err) {
        console.log("Erro: " + err);
        return res
          .status(500)
          .json({ error: "Erro ao obter a descrição da tabela usuário" });
      }

      res.status(200).json({ message: "Descrição da tabela usuário", result });
    });
  }

  /*Consulta para obter a descrição da tabela compra */
  static async descCompra(req, res) {
    const queryDescCompra = "desc compra";

    connect.query(queryDescCompra, async function (err, result, fields) {
      if (err) {
        console.log("Erro: " + err);
        return res
          .status(500)
          .json({ error: "Erro ao obter a descrição da tabela compra" });
      }

      res.status(200).json({ message: "Descrição da tabela compra", result });
    });
  }

   /*Consulta para obter a descrição da tabela evento */
   static async descEvento(req, res) {
    const queryDescEvento = "desc evento";

    connect.query(queryDescEvento, async function (err, result, fields) {
      if (err) {
        console.log("Erro: " + err);
        return res
          .status(500)
          .json({ error: "Erro ao obter a descrição da tabela evento" });
      }

      res.status(200).json({ message: "Descrição da tabela evento", result });
    });
  }

   /*Consulta para obter a descrição da tabela ingresso */
   static async descIngresso(req, res) {
    const queryDescIngresso = "desc ingresso";

    connect.query(queryDescIngresso, async function (err, result, fields) {
      if (err) {
        console.log("Erro: " + err);
        return res
          .status(500)
          .json({ error: "Erro ao obter a descrição da tabela ingresso" });
      }

      res.status(200).json({ message: "Descrição da tabela ingresso", result });
    });
  }

     /*Consulta para obter a descrição da tabela organizador */
     static async descOrganizador(req, res) {
        const queryDescOrganizador = "desc organizador";
    
        connect.query(queryDescOrganizador, async function (err, result, fields) {
          if (err) {
            console.log("Erro: " + err);
            return res
              .status(500)
              .json({ error: "Erro ao obter a descrição da tabela organizador" });
          }
    
          res.status(200).json({ message: "Descrição da tabela organizador", result });
        });
      }

         /*Consulta para obter a descrição da tabela status */
   static async descStatus(req, res) {
    const queryDescStatus = "desc status";

    connect.query(queryDescStatus, async function (err, result, fields) {
      if (err) {
        console.log("Erro: " + err);
        return res
          .status(500)
          .json({ error: "Erro ao obter a descrição da tabela status" });
      }

      res.status(200).json({ message: "Descrição da tabela status", result });
    });
  }
}; /*fim class */
