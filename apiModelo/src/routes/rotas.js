const router = require("express").Router();
const dbController = require('../controller/dbController');


router.get("/sistemaIngressosOnline/", dbController.getTables);
router.get("/sistemaIngressosOnline/description", dbController.getTablesDescription);
router.get("/sistemaIngressosOnline/descriptionUsuario", dbController.descUsuario);
router.get("/sistemaIngressosOnline/descriptionCompra", dbController.descCompra);
router.get("/sistemaIngressosOnline/descriptionEvento", dbController.descEvento);
router.get("/sistemaIngressosOnline/descriptionIngresso", dbController.descIngresso);
router.get("/sistemaIngressosOnline/descriptionOrganizador", dbController.descOrganizador);
router.get("/sistemaIngressosOnline/descriptionStatus", dbController.descStatus);

module.exports = router;
